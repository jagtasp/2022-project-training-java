/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2022/11/9 16:22:29                           */
/*==============================================================*/


drop table if exists board;

drop table if exists friend;

drop table if exists room;

drop table if exists room_message;

drop table if exists room_to_userroom;

drop table if exists star;

drop table if exists system_story;

drop table if exists user;

drop table if exists user_message;

drop table if exists user_room;

drop table if exists user_story;

drop table if exists user_to_userroom;

/*==============================================================*/
/* Table: board                                                 */
/*==============================================================*/
create table board
(
   board_id             varchar(20) not null,
   roo_room_id          varchar(100),
   user_id              varchar(100) not null,
   room_id              varchar(20) not null,
   message_time         datetime not null,
   board_content        varchar(200) not null,
   primary key (board_id)
);

/*==============================================================*/
/* Table: friend                                                */
/*==============================================================*/
create table friend
(
   id                   varchar(20) not null,
   use_user_id          varchar(20),
   user_id              varchar(100) not null,
   friend_id            varchar(100) not null,
   primary key (id)
);

/*==============================================================*/
/* Table: room                                                  */
/*==============================================================*/
create table room
(
   room_id              varchar(100) not null,
   create_time          datetime not null,
   room_name            varchar(20) not null,
   user_count           int not null,
   user_id              varchar(100) not null,
   room_describe        varchar(200),
   primary key (room_id)
);

/*==============================================================*/
/* Table: room_message                                          */
/*==============================================================*/
create table room_message
(
   room_message_id      varchar(20) not null,
   roo_room_id          varchar(100),
   user_room_id         varchar(20),
   room_message_time    datetime not null,
   room_message_type    varchar(20) not null,
   user_id              varchar(100) not null,
   room_id              varchar(20) not null,
   room_message_content varchar(200),
   primary key (room_message_id)
);

/*==============================================================*/
/* Table: room_to_userroom                                      */
/*==============================================================*/
create table room_to_userroom
(
   room_id              varchar(100) not null,
   user_room_id         varchar(20) not null,
   primary key (room_id, user_room_id)
);

/*==============================================================*/
/* Table: star                                                  */
/*==============================================================*/
create table star
(
   stars_id             varchar(20) not null,
   user_id              varchar(20),
   star_id              varchar(100) not null,
   longitude            varchar(20),
   latitude             varchar(20),
   distance             int,
   primary key (stars_id)
);

/*==============================================================*/
/* Table: system_story                                          */
/*==============================================================*/
create table system_story
(
   sysstory_id          varchar(20) not null,
   content              varchar(200) not null,
   primary key (sysstory_id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   user_id              varchar(20) not null,
   stars_id             varchar(20),
   star_id              varchar(100) not null,
   user_password        varchar(20) not null,
   user_phonenumber     varchar(20) not null,
   user_registtime      date not null,
   primary key (user_id)
);

/*==============================================================*/
/* Table: user_message                                          */
/*==============================================================*/
create table user_message
(
   user_message_id      varchar(20) not null,
   id                   varchar(20),
   user_message_time    datetime not null,
   user_message_type    varchar(20) not null,
   user_id              varchar(100) not null,
   friend_id            varchar(100) not null,
   content              varchar(200),
   primary key (user_message_id)
);

/*==============================================================*/
/* Table: user_room                                             */
/*==============================================================*/
create table user_room
(
   user_room_id         varchar(20) not null,
   user_id              varchar(100) not null,
   room_id              varchar(20) not null,
   room_crate           bool not null,
   room_collect         bool not null,
   primary key (user_room_id)
);

/*==============================================================*/
/* Table: user_story                                            */
/*==============================================================*/
create table user_story
(
   story_id             varchar(20) not null,
   sys_sysstory_id      varchar(20),
   sysstory_id          varchar(20) not null,
   user_id              varchar(100) not null,
   time                 datetime not null,
   content              varchar(200),
   primary key (story_id)
);

/*==============================================================*/
/* Table: user_to_userroom                                      */
/*==============================================================*/
create table user_to_userroom
(
   user_id              varchar(20) not null,
   user_room_id         varchar(20) not null,
   primary key (user_id, user_room_id)
);

alter table board add constraint FK_room_to_board foreign key (roo_room_id)
      references room (room_id) on delete restrict on update restrict;

alter table friend add constraint FK_user_to_friend foreign key (use_user_id)
      references user (user_id) on delete restrict on update restrict;

alter table room_message add constraint FK_room_to_roommessage foreign key (roo_room_id)
      references room (room_id) on delete restrict on update restrict;

alter table room_message add constraint FK_userroom_to_roommessage foreign key (user_room_id)
      references user_room (user_room_id) on delete restrict on update restrict;

alter table room_to_userroom add constraint FK_room_to_userroom foreign key (room_id)
      references room (room_id) on delete restrict on update restrict;

alter table room_to_userroom add constraint FK_room_to_userroom2 foreign key (user_room_id)
      references user_room (user_room_id) on delete restrict on update restrict;

alter table star add constraint FK_user_to_stars foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table user add constraint FK_user_to_stars2 foreign key (stars_id)
      references star (stars_id) on delete restrict on update restrict;

alter table user_message add constraint FK_friend_to_usermessage foreign key (id)
      references friend (id) on delete restrict on update restrict;

alter table user_story add constraint FK_sysstory_story foreign key (sys_sysstory_id)
      references system_story (sysstory_id) on delete restrict on update restrict;

alter table user_to_userroom add constraint FK_user_to_userroom foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table user_to_userroom add constraint FK_user_to_userroom2 foreign key (user_room_id)
      references user_room (user_room_id) on delete restrict on update restrict;

